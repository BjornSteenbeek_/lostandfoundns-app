// node
const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const router = require('./routes/web');
const session = require('express-session');

//database
const sequelize = require('./utils/database');
const app = express();

var Routes = require('./routes/web');

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded(
    {extended:false}
));

app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: 'auto' }
}));

app.use(function(req, res, next) {
    res.locals.userSession = req.session.userSession;
    next();
});

const config = require("./config/app.json");
global.prefix = config.environment === "dev" ? "" : "/~bjorn/dagtentamen/64";

app.use(`${prefix}`+'/', router);

// static files
app.use('/', Routes);
app.use(express.static(path.join(__dirname, 'public')));

sequelize.sync()
    .then(result => {
        app.listen(10064); // Only works when .then() is reached
    })
    .catch(err => {
        console.log(err);
    });
