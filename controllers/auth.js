const model = require('../models');
const passwordHash = require('password-hash');

exports.login = (req, res, next) => {

    res.render('auth/login', {
    });
};

exports.loginCheck = (req, res, next) => {
    const email = req.body.email;
    const pincode = req.body.pincode;

    model.User.findOne({where:{ email: email} }).then(user => {
            if (passwordHash.verify(pincode, user.pincode)) {
                req.session.userSession = {
                    loggedIn: true,
                    id: user,
                    role: user.roleId
                };
                res.redirect('/');
            } else {
                res.redirect('/login');
            }
    }).catch(err => {
        console.log(err);
    });
};

exports.register = (req, res, next) => {
    res.render('auth/register', {
    });
};


exports.registerCreate = (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const pincode = req.body.pincode;
    const checkPincode = req.body.checkPincode;
    const roleId = 2;

    var hashedPincode = passwordHash.generate(pincode);

    if(pincode === checkPincode) {
        model.User.create(
            {
                firstName: firstName,
                lastName: lastName,
                email: email,
                pincode: hashedPincode,
                roleId: roleId,
            }).then(result => {
            req.session.userSession = {
                loggedIn: true,
                id: user,
                role: user.roleId
            };
            console.log('User created');
        }).catch(err => {
            console.log(err);
        });

        res.redirect('/');
    } else {
        res.redirect('/');
    }
};

exports.logout = (req, res, next) => {
    req.session.destroy();
    res.redirect('/');
};