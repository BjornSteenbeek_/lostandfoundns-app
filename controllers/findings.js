const model = require('../models');

exports.index = (req, res, next) => {

    model.Findings.findAll().then(findings => {
        res.render('findings/index', {
            findings: findings,
        });
    });
};

exports.create = (req, res, next) => {
    model.User.findAll().then(users => {
        model.Trajects.findAll().then(trajects => {
            res.render('findings/create', {
                users: users,
                trajects: trajects,
            });
        });
    });
};

exports.store = (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const userId = req.body.userId;
    const trajectId = req.body.trajectId;

    model.Findings.create(
        {
            name: name,
            description: description,
            email: email,
            userId: userId,
            trajectId: trajectId,
        }).then(result => {
        console.log('User created');
    }).catch(err => {
        console.log(err);
    });

    res.redirect('/findings/index');
};
exports.show = (req, res, next) => {
    const id = req.params.id;

    model.Findings.findByPk(id).then(finding => {
        model.User.findByPk(finding.userId).then(user => {
            model.Traject.findByPk(finding.trajectId).then(traject => {
                res.render('findings/show', {
                    finding: finding,
                    user: user,
                    traject: traject,
                });
            });
        });
    });
};


exports.edit = (req, res, next) => {
    const id = req.params.id;

    model.User.findByPk(id).then(user => {
        model.Role.findAll().then(roles => {
            res.render('findings/edit', {
                user: user,
                roles: roles,
            });
        });
    });
};

exports.update = (req, res, next) => {
    const id = req.body.id;

    const name = req.body.name;
    const description = req.body.description;
    const userId = req.body.userId;
    const trajectId = req.body.trajectId;

    model.User.update(
        {
            name: name,
            lastName: lastName,
            userId: userId,
            trajectId: trajectId,

        },
        {
            where:
                {
                    id: id
                }
        }
    ).then(result => {
        console.log('Findings updated');
    }).catch(err => {
        console.log(err);
    });

    res.redirect('/findings/index');
};

exports.destroy = (req, res, next) => {
    const id = req.params.id;

    model.Findings.findByPk(id).then(finding => {
        res.render('findings/destroy', {
            finding: finding,
        });
    });
};

exports.delete = (req, res, next) => {
    const id = req.body.id;

    model.Findings.destroy(
        {
            where:
                {
                    id: id
                }
        }
    ).then(result => {
        console.log('findings deleted');
    }).catch(err => {
        console.log(err);
    });

    res.redirect('/findings/index');
};