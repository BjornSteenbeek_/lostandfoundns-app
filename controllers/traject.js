const model = require('../models');

exports.index = (req, res, next) => {

    model.User.findAll().then(users => {
        model.Role.findAll().then(roles => {
            res.render('users/index', {
                users: users,
                roles: roles,
            });
        });
    });
};

exports.create = (req, res, next) => {
    model.Role.findAll().then(roles => {
        res.render('users/create', {
            roles: roles,
        });
    });
};

exports.store = (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const pincode = req.body.pincode;
    const roleId = req.body.roleId;

    model.User.create(
        {
            firstName: firstName,
            lastName: lastName,
            email: email,
            pincode: pincode,
            roleId: roleId,
        }).then(result => {
        console.log('User created');
    }).catch(err => {
        console.log(err);
    });

    res.redirect('/users/index');
};
exports.show = (req, res, next) => {
    const id = req.params.id;

    model.User.findByPk(id).then(user => {
        model.Role.findByPk(user.roleId).then(role => {
            res.render('users/show', {
                user: user,
                role: role,
            });
        });
    });
};


exports.edit = (req, res, next) => {
    const id = req.params.id;

    model.User.findByPk(id).then(user => {
        model.Role.findAll().then(roles => {
            res.render('users/edit', {
                user: user,
                roles: roles,
            });
        });
    });
};

exports.update = (req, res, next) => {
    const id = req.body.id;

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const pincode = req.body.pincode;
    const roleId = req.body.roleID;

    model.User.update(
        {
            firstName: firstName,
            lastName: lastName,
            email: email,
            pincode: pincode,
            roleId: roleId,

        },
        {
            where:
                {
                    id: id
                }
        }
    ).then(result => {
        console.log('Users updated');
    }).catch(err => {
        console.log(err);
    });

    res.redirect('/users/index');
};

exports.destroy = (req, res, next) => {
    const id = req.params.id;

    model.User.findByPk(id).then(user => {
        res.render('users/destroy', {
            user: user,
        });
    });
};

exports.delete = (req, res, next) => {
    const id = req.body.id;

    model.User.destroy(
        {
            where:
                {
                    id: id
                }
        }
    ).then(result => {
        console.log('User deleted');
    }).catch(err => {
        console.log(err);
    });

    res.redirect('/users/index');
};