'use strict';
module.exports = (sequelize, DataTypes) => {
  const Findings = sequelize.define('Findings', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    findingId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    trajectId: DataTypes.INTEGER
  }, {});
  Findings.associate = function(models) {
    // associations can be defined here
  };
  return Findings;
};