'use strict';
module.exports = (sequelize, DataTypes) => {
  const FindingsPropteries = sequelize.define('FindingsPropteries', {
    propertyId: DataTypes.INTEGER,
    findingId: DataTypes.INTEGER
  }, {});
  FindingsPropteries.associate = function(models) {
    // associations can be defined here
  };
  return FindingsPropteries;
};