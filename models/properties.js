'use strict';
module.exports = (sequelize, DataTypes) => {
  const Properties = sequelize.define('Properties', {
    property: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {});
  Properties.associate = function(models) {
    // associations can be defined here
  };
  return Properties;
};