'use strict';
module.exports = (sequelize, DataTypes) => {
  const Traject = sequelize.define('Trajects', {
    startRoute: DataTypes.STRING,
    endRoute: DataTypes.STRING
  }, {});
  Traject.associate = function(models) {
    // associations can be defined here
  };
  return Traject;
};