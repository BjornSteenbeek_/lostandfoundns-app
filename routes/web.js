const express = require('express');
const web = express.Router();

// Require controller modules.
var findingsRoutes = require('../controllers/findings');
var usersRoutes = require('../controllers/users');
var authRoutes = require('../controllers/auth');
var homeRoutes = require('../controllers/home');
var propertiesRoutes = require('../controllers/properties');
var trajectRoutes = require('../controllers/traject');

//GETS
web.get('/', homeRoutes.index);
//Findings
web.get('/findings/index', findingsRoutes.index);
web.get('/findings/create', findingsRoutes.create);
web.get('/findings/show/:id', findingsRoutes.show);
web.get('/findings/edit/:id', findingsRoutes.edit);
web.get('/findings/destroy/:id', findingsRoutes.destroy);
//Users
web.get('/users/index', usersRoutes.index);
web.get('/users/create', usersRoutes.create);
web.get('/users/show/:id', usersRoutes.show);
web.get('/users/edit/:id', usersRoutes.edit);
web.get('/users/destroy/:id', usersRoutes.destroy);
//LoginRegister
web.get('/login', authRoutes.login);
web.get('/register', authRoutes.register);
web.get('/logout', authRoutes.logout);
//Properties
web.get('/properties/index', propertiesRoutes.index);
web.get('/properties/create', propertiesRoutes.create);
web.get('/properties/show/:id', propertiesRoutes.show);
web.get('/properties/edit/:id', propertiesRoutes.edit);
web.get('/properties/destroy/:id', propertiesRoutes.destroy);
//Traject
web.get('/traject/index', trajectRoutes.index);
web.get('/traject/create', trajectRoutes.create);
web.get('/traject/show/:id', trajectRoutes.show);
web.get('/traject/edit/:id', trajectRoutes.edit);
web.get('/traject/destroy/:id', trajectRoutes.destroy);

//POSTS
//Findings
web.post('/findings/store', findingsRoutes.store);
web.post('/findings/update', findingsRoutes.update);
web.post('/findings/delete', findingsRoutes.delete);
//Users
web.post('/users/store', usersRoutes.store);
web.post('/users/update', usersRoutes.update);
web.post('/users/delete', usersRoutes.delete);
//LoginRegister
web.post('/loginCheck', authRoutes.loginCheck);
web.post('/registerCreate', authRoutes.registerCreate);
//Properties
web.post('/properties/store', propertiesRoutes.store);
web.post('/properties/update', propertiesRoutes.update);
web.post('/properties/delete', propertiesRoutes.delete);
//Traject
web.post('/traject/store', trajectRoutes.store);
web.post('/traject/update', trajectRoutes.update);
web.post('/traject/delete', trajectRoutes.delete);


module.exports = web;
