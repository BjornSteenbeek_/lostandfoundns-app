'use strict';

const passwordHash = require('password-hash');

module.exports = {
  up: (queryInterface, Sequelize) => {
      // Add altering commands here.
      // Return a promise to correctly handle asynchronicity.

      // Example:
      return queryInterface.bulkInsert('Users', [
          {
              firstName: 'Admin',
              lastName: 'Admin',
              email: 'Admin@Admin.com',
              pincode: passwordHash.generate('12345'),
              roleId: 3,
              createdAt : new Date(),
              updatedAt : new Date()
          },
          {
              firstName: 'John',
              lastName: 'Doe',
              email: 'JohnDoe@hotmail.com',
              pincode: passwordHash.generate('12345'),
              roleId: 2,
              createdAt : new Date(),
              updatedAt : new Date()
          },
          {
              firstName: 'Bjorn',
              lastName: 'Steenbeek',
              email: 'bjornsteenbeek@hotmail.com',
              pincode: passwordHash.generate('12345'),
              roleId: 3,
              createdAt : new Date(),
              updatedAt : new Date()
          },
          {
              firstName: 'Mauro',
              lastName: 'Bertozzi',
              email: 'Maurobertozzi@hotmail.com',
              pincode: passwordHash.generate('12345'),
              roleId: 1,
              createdAt: new Date(),
              updatedAt: new Date()
          },
          {
              firstName: 'Tim',
              lastName: 'de Haas',
              email: 'Timdehaas@hotmail.com',
              pincode: passwordHash.generate('12345'),
              roleId: 2,
              createdAt: new Date(),
              updatedAt: new Date()
          },
          {
              firstName: 'Thom',
              lastName: 'Kok',
              email: 'Thomkok@hotmail.com',
              pincode: passwordHash.generate('12345'),
              roleId: 2,
              createdAt: new Date(),
              updatedAt: new Date()
          }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
      // Add reverting commands here.
      // Return a promise to correctly handle asynchronicity.

      // Example:
      return queryInterface.bulkDelete('Users', null, {});
  }
};
