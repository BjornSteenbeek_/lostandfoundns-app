'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      // Add altering commands here.
      // Return a promise to correctly handle asynchronicity.
      //
      // Example:
      return queryInterface.bulkInsert('Roles', [
        {
            role: 'user',
            createdAt: new Date(),
            updatedAt: new Date()
        },
         {
              role: 'werknemer',
              createdAt: new Date(),
              updatedAt: new Date()
          },
        {
            role: 'admin',
            createdAt: new Date(),
            updatedAt: new Date()
        }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
      // Add reverting commands here.
      // Return a promise to correctly handle asynchronicity.
      //
      // Example:
      return queryInterface.bulkDelete('Roles', null, {});
  }
};
