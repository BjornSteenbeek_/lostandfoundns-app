'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Add altering commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkInsert('Findings', [
      {
        Name: 'Rugzak',
        Description: 'Een rugzak met een dell laptop erin',
        userId: null,
        trajectId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Name: 'Boek',
        Description: 'Een boek genaamt het geheim',
        userId: null,
        trajectId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },      {
        Name: 'Jas',
        Description: 'Een jas met een IPhone erin',
        userId: null,
        trajectId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    // Add reverting commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
