'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Add altering commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkInsert('FindingsProperties', [
      {
        propertyId: 1,
        findingId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        propertyId: 2,
        findingId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        propertyId: 3,
        findingId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        propertyId: 4,
        findingId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        propertyId: 5,
        findingId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    // Add reverting commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
