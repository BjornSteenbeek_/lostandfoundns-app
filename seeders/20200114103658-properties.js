'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Add altering commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkInsert('properties', [
      {
        property: "geel",
        description: "Dit voorwerp is geel",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        property: "groot",
        description: "Dit voorwerp is groot",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        property: "rood",
        description: "Dit voorwerp is rood",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        property: "klein",
        description: "Dit voorwerp is klein",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        property: "blauw",
        description: "Dit voorwerp is blauw",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    // Add reverting commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
