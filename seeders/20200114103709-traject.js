'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Add altering commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkInsert('Traject', [
      {
        startRoute: 'Amsterdam Centraal',
        EndRoute: 'Utrecht Centraal',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        startRoute: 'Almere Centrum',
        EndRoute: 'Utrecht Centraal',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        startRoute: 'Utrecht Centraal',
        EndRoute: 'Almere Centrum',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        startRoute: 'Groningen Centraal',
        EndRoute: 'Utrecht Centraal',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        startRoute: 'Utrecht Centraal',
        EndRoute: 'Grondingen Centraal',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        startRoute: 'Utrecht Centraal',
        EndRoute: 'Amsterdam Centraal',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    // Add reverting commands here.
    // Return a promise to correctly handle asynchronicity.
    //
    // Example:
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
